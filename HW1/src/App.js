import React, { Component } from "react";
import Button from "./components/Button";
import Modal from "./components/Modal";

import "./index.scss";

class App extends Component {
  state = {
    first: {
      backgroundColor: "#CC2800",
      text: "Xsfpoivj vsdpivj dspovjdvsih vdspoihdv hpsidhv. Fsoihgr oiuhgw weiuf goih fh yesfdgerhs hsefg, soihf",
      header: "Do you shure to close FIRST window?",
    },

    second: {
      backgroundColor: "#D49C05",
      text: "Ksofdivh erg hoiuh. Fsgrh eofih! Jsoihdgrw grh soiurhgwre. Fosihre gwrgrg georgh. Lsigre hfef ghb e?",
      header: "Do you shure to close SECOND window?",
    },

    firstModal: false,
    secondModal: false,
  };

  showModalFirst = () => {
    this.setState({ firstModal: true });
  };

  showModalSecond = () => {
    this.setState({ secondModal: true });
  };

  closeModal = () => {
    this.setState({ firstModal: false });
    this.setState({ secondModal: false });
  };

  render() {
    return (
      <>
        <Button
          text="Open First Window"
          backgroundColor="#1e434c"
          onClick={() => this.showModalFirst()}
        />
        <Button
          text="Open Second Window"
          backgroundColor="#9b4f0f"
          onClick={() => this.showModalSecond()}
        />

        {this.state.firstModal && (
          <Modal
            backgroundColor={this.state.first.backgroundColor}
            text={this.state.first.text}
            header={this.state.first.header}
            onClick={() => this.closeModal()}
            actions={
              <div className="buttons-wrap">
                <Button
                  text="OK"
                  backgroundColor="#B21F00"
                  onClick={() => this.closeModal()}
                />
                <Button
                  text="Cancel"
                  backgroundColor="#B21F00"
                  onClick={() =>
                    alert("Please, press OK button to close modal window")
                  }
                />
                <svg
                  height="20px"
                  width="20px"
                  viewBox="0 0 500 500"
                  onClick={() => this.closeModal()}
                >
                  <path d="M505.943,6.058c-8.077-8.077-21.172-8.077-29.249,0L6.058,476.693c-8.077,8.077-8.077,21.172,0,29.249    C10.096,509.982,15.39,512,20.683,512c5.293,0,10.586-2.019,14.625-6.059L505.943,35.306    C514.019,27.23,514.019,14.135,505.943,6.058z" />

                  <path d="M505.942,476.694L35.306,6.059c-8.076-8.077-21.172-8.077-29.248,0c-8.077,8.076-8.077,21.171,0,29.248l470.636,470.636    c4.038,4.039,9.332,6.058,14.625,6.058c5.293,0,10.587-2.019,14.624-6.057C514.018,497.866,514.018,484.771,505.942,476.694z" />
                </svg>
              </div>
            }
          />
        )}
        {this.state.secondModal && (
          <Modal
            backgroundColor={this.state.second.backgroundColor}
            text={this.state.second.text}
            header={this.state.second.header}
            onClick={() => this.closeModal()}
            actions={
              <div className="buttons-wrap">
                <Button
                  text="OK"
                  backgroundColor="#9D6B00"
                  onClick={() => this.closeModal()}
                />
                <Button
                  text="Cancel"
                  backgroundColor="#9D6B00"
                  onClick={() =>
                    alert("Please, press OK button to close modal window")
                  }
                />
                <svg
                  height="20px"
                  width="20px"
                  viewBox="0 0 500 500"
                  onClick={() => this.closeModal()}
                >
                  <path d="M505.943,6.058c-8.077-8.077-21.172-8.077-29.249,0L6.058,476.693c-8.077,8.077-8.077,21.172,0,29.249    C10.096,509.982,15.39,512,20.683,512c5.293,0,10.586-2.019,14.625-6.059L505.943,35.306    C514.019,27.23,514.019,14.135,505.943,6.058z" />

                  <path d="M505.942,476.694L35.306,6.059c-8.076-8.077-21.172-8.077-29.248,0c-8.077,8.076-8.077,21.171,0,29.248l470.636,470.636    c4.038,4.039,9.332,6.058,14.625,6.058c5.293,0,10.587-2.019,14.624-6.057C514.018,497.866,514.018,484.771,505.942,476.694z" />
                </svg>
              </div>
            }
          />
        )}
      </>
    );
  }
}

export default App;
