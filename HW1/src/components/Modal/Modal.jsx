import React, { Component } from "react";
import "./Modal.scss";

class Modal extends Component {
  render() {
    const { onClick, backgroundColor, text, header, actions } = this.props;

    return (
      <>
        <div className="modal" onClick={onClick}>
          <div
            className="modal-block"
            style={{ backgroundColor: backgroundColor }}
            onClick={(e) => e.stopPropagation()}
          >
            <div className="modal-header">{header}</div>
            <p className="modal-text">{text}</p>
            {actions}
          </div>
        </div>
      </>
    );
  }
}

export default Modal;
